export default class {
  constructor(year, title, desc) {
    this._year = year;
    this._title = title;
    this._desc = desc;
  }

  get year() {
    return this._year;
  }

  get title() {
    return this._title;
  }

  get description() {
    return this._desc;
  }

  set year(y) {
    this._year = y;
  }

  set title(t) {
    this._t = t;
  }

  set description(d) {
    this._desc = d;
  }
}
