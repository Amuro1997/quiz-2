import Person from './Person';
import $ from 'jquery';
function fetchData(url) {
  return fetch(url).then(response => {
    return response.json().then(data => data);
  });
}

function generateLine(arr) {
  return `<tr>
      <td class="year">${arr.year}</td>
      <td class="introBlock">
        <h4>${arr.title}</h4>
        <p>${arr.description}</p>
      </td>
    </tr>`;
}
const URL = 'http://localhost:3000/person';
fetchData(URL).then(result => {
  let p = new Person(
    result.name,
    result.age,
    result.description,
    result.educations
  );
  $('#name').html(p.name);
  $('#age').html(p.age);
  $('#intro').html(p.description);
  p.education.map(ed => {
    console.log(ed);
    $('#EducationTable').append(generateLine(ed));
  });
});
