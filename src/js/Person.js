import Education from './Education';

export default class {
  constructor(name, age, desc, edu) {
    this._name = name;
    this._age = age;
    this._description = desc;
    this._education = edu;
  }

  get name() {
    return this._name;
  }

  get age() {
    return this._age;
  }

  get description() {
    return this._description;
  }

  get education() {
    return this._education;
  }

  set name(n) {
    this._name = n;
  }

  set age(a) {
    this._age = a;
  }

  set description(d) {
    this._description = d;
  }

  set education(e) {
    this._education = [];
    for (let i = 0; i < e.length; ++i)
      this._education.push(
        new Education(e[i].year, e[i].age, e[i].description)
      );
  }
}
